#!/bin/sh

cd rc-challenge-tiles

for D in */
    do
        cd "$D"
        echo "Converting" "$D" "contents to JPG..."
        mogrify -format jpg *.png && rm *.png
        cd ..
done

echo "Done."

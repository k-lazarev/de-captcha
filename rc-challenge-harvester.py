import time
from io import BytesIO
from subprocess import call

from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

firefoxCapabilities = DesiredCapabilities.FIREFOX
firefoxCapabilities["marionette"] = True
firefoxCapabilities["handleAlerts"] = True
firefoxCapabilities["acceptSslCerts"] = True
firefoxCapabilities["acceptInsecureCerts"] = False

binary = FirefoxBinary("/home/k-lazarev/dev/firefox/firefox")
geckoPath = "/home/k-lazarev/dev/geckodriver/geckodriver"
profile = "/home/k-lazarev/dev/firefox-profile"

startTime = time.time()
cycleNumber = 1


def harvester():
    browser = webdriver.Firefox(firefox_profile=profile, firefox_binary=binary, capabilities=firefoxCapabilities,
                                executable_path=geckoPath)
    browser.get("https://www.google.com/recaptcha/api2/demo")

    def extractor():
        global cycleNumber
        while True:
            browser.refresh()
            submitButton = browser.find_element_by_xpath(
                "/html/body/div[1]/form/fieldset/ul/li[5]/div/div/div/div/iframe")
            submitButton.click()
            time.sleep(1)
            browser.switch_to.frame(1)
            imgNote = browser.find_element_by_xpath("/html/body/div/div/div[2]/div[1]/div[1]/div/strong")
            imgNoteText = imgNote.text
            rcTiles = browser.find_elements_by_class_name("rc-image-tile-wrapper")

            for tile in rcTiles:
                location = tile.location
                size = tile.size
                png = browser.get_screenshot_as_png()
                im = Image.open(BytesIO(png))
                left = location['x']
                top = location['y']
                right = location['x'] + size['width']
                bottom = location['y'] + size['height']

                im = im.crop((left, top, right, bottom))
                im.save("/home/k-lazarev/dev/gitlab/de-captcha/rc-challenge-tiles/" + imgNoteText + "/" + str(tile)[
                                                                                                          115:-3] + ".png")
            cycleNumber = cycleNumber + 1
            if cycleNumber % 10 == 0:
                print("Execution time: %s seconds, " % (time.time() - startTime) + "loop #" + str(cycleNumber))
            elif cycleNumber % 100 == 0:
                browser.quit()

    try:
        extractor()
    except:
        browser.quit()
        call(["/home/k-lazarev/dev/gitlab/scripts-network/vpn-randomizer.sh"])
        time.sleep(1)


while True:
    harvester()
